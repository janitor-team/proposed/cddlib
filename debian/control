Source: cddlib
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>
Homepage: https://github.com/cddlib/cddlib
Build-Depends: debhelper (>= 13), debhelper-compat (= 13), libgmp-dev, latexmk, latex2html
# latex2html is required due to html.sty.
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/math-team/cddlib.git
Vcs-Browser: https://salsa.debian.org/math-team/cddlib

Package: libcdd-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libgmp-dev, libcdd0d (= ${binary:Version})
Suggests: libcdd-doc
Multi-Arch: same
Description: Library for finding vertices of convex polytopes, development
 The cddlib library is a C library for manipulating general convex
 polyhedra.  It supports converting between the system of linear
 inequalities representation and a vertices and extreme rays
 representation of a polyhedron, and also supports solving linear
 programming problems.
 .
 This package contains the cddlib development files.

Package: libcdd0d
Section: libs
Architecture: any
Conflicts: libcdd0
Depends: ${shlibs:Depends}, ${misc:Depends}, libgmp-dev
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: Library for calculations with convex polytopes, runtime
 The cddlib library is a C library for manipulating general convex
 polyhedra.  It supports converting between the system of linear
 inequalities representation and a vertices and extreme rays
 representation of a polyhedron, and also supports solving linear
 programming problems.
 .
 This package contains the cddlib shared libraries.

Package: libcdd-tools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: libcdd-doc
Replaces: libcdd-test (<< 094h-1~)
Breaks: libcdd-test (<< 094h-1~)
Description: Programs using libcdd
 The cddlib library is a C library for manipulating general convex
 polyhedra.  It supports converting between the system of linear
 inequalities representation and a vertices and extreme rays
 representation of a polyhedron, and also supports solving linear
 programming problems.
 .
 This package contains example programs using libcdd, for example
 cdd_both_reps, which computes minimal H- and V-representations
 of a given convex polytope.

Package: libcdd-doc
Architecture: all
Section: doc
Depends: ${shlibs:Depends}, ${misc:Depends}
Replaces: libcdd-dev (<< 094g-3)
Breaks: libcdd-dev (<< 094g-3)
Multi-Arch: foreign
Description: documentation for libcdd
 The cddlib library is a C library for manipulating general convex
 polyhedra.  It supports converting between the system of linear
 inequalities representation and a vertices and extreme rays
 representation of a polyhedron, and also supports solving linear
 programming problems.
 .
 This package contains the cddlib reference manual.
